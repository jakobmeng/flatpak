Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Flatpak
Source: https://github.com/flatpak/flatpak/releases

Files:
 *
Copyright:
 © 1995-2017 Free Software Foundation, Inc.
 © 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 © 2004-2022 Red Hat, Inc
 © 2004-2010 Christian Persch
 © 2004 Hidetoshi Tajima
 © 2006-2019 Matthias Clasen
 © 2006 Padraig O'Briain
 © 2007-2011 Lennart Poettering
 © 2010 Codethink Limited
 © 2010 Lennart Poettering
 © 2011-2019 Colin Walters
 © 2013-2024 Collabora Ltd.
 © 2013 Allison Karlitskaya
 © 2014-2018 Alexander Larsson
 © 2016-2018 Canonical Ltd.
 © 2016 Piotr Drag
 © 2016 Aviary.pl
 © 2016 Zbigniew Jędrzejewski-Szmek
 © 2016 Wolfgang Stöggl
 © 2016 Mario Blättermann
 © 2017 Daniel Rusek
 © 2017 Roman Kharin
 © 2017 Patrick Griffis
 © 2017 Endless, Inc.
 © 2018-2019 Endless Mobile, Inc.
 © 2017-2022 Endless OS Foundation LLC
 © 2018 Peter Wu
 © 2019 Emmanuel Fleury
 © 2019 Ting-Wei Lan
 © 2019-2021 Matthew Leeds
 © 2019 Sebastian Schwarz
 © 2020 Matt Rose
 © 2021 Simon McVittie
 © 2021 Joshua Lee
 © 2021 Casper Dik
 © 2022 Alexander Richardson
 © 2022 Ray Strode
 © 2022 Thomas Haller
 © 2023 Sebastian Wilhelmi
 © 2023 CaiJingLong
License: LGPL-2+ and LGPL-2.1+

Files:
 common/valgrind-private.h
Copyright:
 © 2000-2017 Julian Seward
License: bzip2-1.0.6

Files:
 debian/*
Copyright:
 © 2015 David King <amigadave@amigadave.com>
 © 2016-2023 Collabora Ltd.
License: LGPL-2.1+

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
Comment:
 On Debian systems, the full text of the GNU Lesser General Public License
 version 2 can be found in the file '/usr/share/common-licenses/LGPL-2'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
Comment:
 On Debian systems, the full text of the GNU Lesser General Public License
 version 2.1 can be found in the file '/usr/share/common-licenses/LGPL-2.1'.

License: bzip2-1.0.6
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. The origin of this software must not be misrepresented; you must
    not claim that you wrote the original software.  If you use this
    software in a product, an acknowledgment in the product
    documentation would be appreciated but is not required.
 .
 3. Altered source versions must be plainly marked as such, and must
    not be misrepresented as being the original software.
 .
 4. The name of the author may not be used to endorse or promote
    products derived from this software without specific prior written
    permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
